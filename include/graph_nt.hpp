#ifndef __GRAPH_HPP__
#define __GRAPH_HPP__

#include <exception>
#include <map>
#include <vector>
#include <string>
#include <algorithm>

// Searches an element in the container, if it exists, it is removed
template<class C, class E>
void find_remove(C &cont, E elem)
{
    auto pos = cont.find(elem);
    if (pos != end(cont))
        cont.erase(pos);
}

// Exception : wrong node identifier
class NodeNotFound {
    const int id;
public:
    NodeNotFound(int node_id) : id(node_id) {}
    std::string msg() const {
        std::string m = "Node " + std::to_string(id) + " not found";
        return m;
    }
};

// Exception : wrong node identifier 
class EdgeNotFound {
    const int id;
public:
    EdgeNotFound(int edge_id) : id(edge_id) {}
    std::string msg() const {
        std::string m = "Edge " + std::to_string(id) + " not found";
        return m;
    }
};

class Graph {
    struct Node {
        int node_id;
        std::string data;
    };
    struct Edge {
        int edge_id;
        std::string data;
        int source_id;
        int dest_id;
    };

    /* data structures */
    std::map<int, Node> nodes;
    std::map<int, Edge> edges;
    std::map<int, std::vector<int>> dests;
    int id_counter;
    int edge_counter;

public:

    Graph() { /*todo*/ }
    Graph(const Graph &other) { /* todo */ }

    inline int add_node(const std::string &m) { /* todo */ return -1; }

    inline bool node_exist(int id) const { /* todo */ return false; }

    inline int add_edge(const std::string &m, int source_id, int dest_id)
        { /* todo */ return -1;}
    
    inline void remove_node(int node_id) { /* todo */ }

    inline int search_node(const std::string &m) const { /* todo */ return 0;}
    
    inline std::string get_node_data(int node_id) const { /* todo */ return "TODO";}

    inline std::string get_edge_data(int edge_id) const { /* todo */ return "TODO";}

    inline int get_edge_source(int edge_id) const { /* todo */ return -1;}
    
    inline int get_edge_dest(int edge_id) const { /* todo */ return -1;}

    std::vector<int> get_successors(int node_id) const { /* todo */ return std::vector<int>();}
    
    std::vector<int> get_predecessors(int node_id) const { /* todo */ return std::vector<int>();}

    using Path=std::vector<int>;

    std::vector<Path> all_paths(int from, int to) const
        { /* todo */ return std::vector<Path>();}

    Path shortest_path(int from, int to) const
        { /* todo */ return Path{};}
};


#endif
